﻿namespace Tiler
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblTimer = new System.Windows.Forms.Label();
            this.numTimer = new System.Windows.Forms.NumericUpDown();
            this.chkTileWindowsVertically = new System.Windows.Forms.RadioButton();
            this.chkTileWindowsHorizontally = new System.Windows.Forms.RadioButton();
            this.butStop = new System.Windows.Forms.Button();
            this.butStart = new System.Windows.Forms.Button();
            this.tmrTiler = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numTimer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTimer
            // 
            this.lblTimer.Location = new System.Drawing.Point(99, 119);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(48, 26);
            this.lblTimer.TabIndex = 11;
            this.lblTimer.Text = "Timer";
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimer
            // 
            this.numTimer.Location = new System.Drawing.Point(147, 119);
            this.numTimer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTimer.Name = "numTimer";
            this.numTimer.Size = new System.Drawing.Size(86, 22);
            this.numTimer.TabIndex = 10;
            this.numTimer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chkTileWindowsVertically
            // 
            this.chkTileWindowsVertically.Location = new System.Drawing.Point(99, 82);
            this.chkTileWindowsVertically.Name = "chkTileWindowsVertically";
            this.chkTileWindowsVertically.Size = new System.Drawing.Size(230, 28);
            this.chkTileWindowsVertically.TabIndex = 9;
            this.chkTileWindowsVertically.Text = "Tile Windows Vertically";
            // 
            // chkTileWindowsHorizontally
            // 
            this.chkTileWindowsHorizontally.Checked = true;
            this.chkTileWindowsHorizontally.Location = new System.Drawing.Point(99, 54);
            this.chkTileWindowsHorizontally.Name = "chkTileWindowsHorizontally";
            this.chkTileWindowsHorizontally.Size = new System.Drawing.Size(230, 28);
            this.chkTileWindowsHorizontally.TabIndex = 8;
            this.chkTileWindowsHorizontally.TabStop = true;
            this.chkTileWindowsHorizontally.Text = "Tile Windows Horizontally";
            this.chkTileWindowsHorizontally.CheckedChanged += new System.EventHandler(this.chkTileWindowsHorizontally_CheckedChanged);
            // 
            // butStop
            // 
            this.butStop.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.butStop.Location = new System.Drawing.Point(217, 156);
            this.butStop.Name = "butStop";
            this.butStop.Size = new System.Drawing.Size(90, 26);
            this.butStop.TabIndex = 7;
            this.butStop.Text = "Stop";
            this.butStop.Click += new System.EventHandler(this.butStop_Click);
            // 
            // butStart
            // 
            this.butStart.Location = new System.Drawing.Point(121, 156);
            this.butStart.Name = "butStart";
            this.butStart.Size = new System.Drawing.Size(90, 26);
            this.butStart.TabIndex = 6;
            this.butStart.Text = "Start";
            this.butStart.Click += new System.EventHandler(this.butStart_Click);
            // 
            // tmrTiler
            // 
            this.tmrTiler.Tick += new System.EventHandler(this.tmrTiler_Tick);
            // 
            // frmMain
            // 
            this.AcceptButton = this.butStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.butStop;
            this.ClientSize = new System.Drawing.Size(425, 238);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.numTimer);
            this.Controls.Add(this.chkTileWindowsVertically);
            this.Controls.Add(this.chkTileWindowsHorizontally);
            this.Controls.Add(this.butStop);
            this.Controls.Add(this.butStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Tiler";
            ((System.ComponentModel.ISupportInitialize)(this.numTimer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblTimer;
        internal System.Windows.Forms.NumericUpDown numTimer;
        internal System.Windows.Forms.RadioButton chkTileWindowsVertically;
        internal System.Windows.Forms.Button butStop;
        internal System.Windows.Forms.Button butStart;
        private System.Windows.Forms.Timer tmrTiler;
        private System.Windows.Forms.RadioButton chkTileWindowsHorizontally;
    }
}

