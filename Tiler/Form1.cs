﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tiler
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void tmrTiler_Tick(object sender, EventArgs e)
        {
            try
            {
                Shell32.Shell shell = new Shell32.Shell();
                if (this.chkTileWindowsHorizontally.Checked)
                {
                    shell.TileHorizontally();
                }
                else if (this.chkTileWindowsVertically.Checked)
                {
                    shell.TileVertically();
                }
            }
            catch
            {

            }
        }

        private void butStart_Click(object sender, EventArgs e)
        {
            tmrTiler.Interval = (int)(numTimer.Value * 1000);
            tmrTiler.Enabled = true;
        }

        private void butStop_Click(object sender, EventArgs e)
        {
            tmrTiler.Enabled = false;
        }

        private void chkTileWindowsHorizontally_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
